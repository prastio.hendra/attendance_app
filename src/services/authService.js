import bcrypt from 'bcrypt';

import { ClientError } from '../libs/errors';
import jwt from '../libs/jwt';

const service = (UserRepository) => ({
  login: async (username, password) => {
    const user = await UserRepository.findOne({ where: { username }, attributes: ['password'] });
    const validatePassword = async function (password) {
      return bcrypt.compare(password, user.password);
    };

    if (user && (await validatePassword(password))) {
      const token = jwt.generateToken(user);

      return token;
    }

    throw new ClientError('Username and/or Password wrong');
  },

  logout: async (incomingToken) => {
    const { id } = incomingToken;

    return UserDeviceRepository.update({ token: null }, { where: { userId: id }, user: { id } });
  },
});

export default service;
