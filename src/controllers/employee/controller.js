const Controller = repositories => {
    const { EmployeeRepository, UserRepository, ExpertisesEmployeeRepository } = repositories;

    const index = async (req, res, next) => {
        try {
            const employees = await EmployeeRepository.findAll({ include: ['user', 'expertises'] })
            res.status(200).json({
                data: employees,
                code: 200,
                message: 'Success'
            });
        } catch (err) {
            next(err)
        }
    }

    const store = async (req, res, next) => {
        const { body } = req;
        try {
            const [employee, created] = await EmployeeRepository.findOrCreate({ where: body });
            if(created) {
                res.status(201).json({
                    data: employee,
                    code: 201,
                    message: 'Employee created successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'The Employee already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }

    const show = async (req, res, next) => {
        const { id } = req.params;
        try {
            const employee = await EmployeeRepository.findByPk(id, { include: ['user', 'expertises'] });
            if(!employee) {
                res.status(404).json({
                    code: 404,
                    message: "Employee doesn't exists"
                });
            } else {
                res.status(200).json({
                    data: employee,
                    code: 200,
                    message: 'Success'
                });
            }
        } catch (err) {
            next(err);
        }
    }

    const update = async (req, res, next) => {
        const { id } = req.params;
        const { body } = req;

        try {
            const employeeExists = await EmployeeRepository.findByPk(id)
            if (employeeExists) {
                const updatedEmployee = await employeeExists.update({ ...body, id }, { returning: true, plain: true });
                res.status(200).json({
                    data: { ...updatedEmployee.dataValues },
                    code: 200,
                    message: 'Success'
                });
            } else {
                res.status(404).json({ code: 404, message: "Employee doesn't exists!" })
            }
        } catch (err) {
            next(err);
        }
    }

    const destroy = async (req, res, next) => {
        const { id } = req.params;
        try {
            const employeeExists = await EmployeeRepository.findByPk(id)

            if (employeeExists) {
                await employeeExists.destroy()
                res.status(200).json({ code: 200, message: 'Employee deleted successfully.' });
            } else {
                res.status(404).json({ message: `There is no Employee with id ${id}` });
            }
        } catch (err) {
            next(err)
        }
    }

    const createUser = async (req, res, next) => {
        const { body } = req;

        try {
            const [user, created] = await UserRepository.findOrCreate({ where: body });
            if(created) {
                res.status(201).json({
                    data: user,
                    code: 201,
                    message: 'User created successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'User already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }

    const expertise = async (req, res, next) => {
        const { id } = req.params;
        const { body } = req;

        try {
            const [expertisesEmployee, created] = await ExpertisesEmployeeRepository.findOrCreate({ where: body });
            if(created) {
                res.status(201).json({
                    data: expertisesEmployee,
                    code: 201,
                    message: 'Employee expertise was assigned successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'Employee expertise already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }

    return {
        index,
        store,
        show,
        update,
        destroy,
        createUser,
        expertise
    }
}

export default Controller;