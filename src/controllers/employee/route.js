import { Router } from 'express';

import { Employee as EmployeeRepository, User as UserRepository, ExpertisesEmployee as ExpertisesEmployeeRepository } from '../../models';
import Controller from './controller';

const basePath = `/api/v1/employee`;
const setPath = path => `${basePath}/${path}`;
const routes = Router();
const controller = Controller({ EmployeeRepository, UserRepository, ExpertisesEmployeeRepository });

routes
    .route(basePath)
    .get(controller.index)
    .post(controller.store);

routes
    .route(setPath(':id'))
    .get(controller.show)
    .put(controller.update)
    .delete(controller.destroy);

routes
    .route(setPath('create-user'))
    .post(controller.createUser);

routes
    .route(setPath('expertise'))
    .post(controller.expertise);

export default routes;
