const Controller = repositories => {
    const { AttendaceRepository } = repositories;

    const checkin = async (req, res, next) => {
        const { path } = req.file;
        const { body } = req;

        try {
            const [projectTeam, created] = await AttendaceRepository.create({ selfie: path, ...body });
            if(created) {
                res.status(201).json({
                    data: projectTeam,
                    code: 201,
                    message: 'Project Teams created successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'The Project Teams already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }

    const checkout = async (req, res, next) => {
        const { body } = req;

        try {
            const [projectTeam, created] = await AttendaceRepository.create({ selfie: path, ...body });
            if(created) {
                res.status(201).json({
                    data: projectTeam,
                    code: 201,
                    message: 'Project Teams created successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'The Project Teams already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }


    return {
        checkin,
        checkout,
        // info
    }
}

export default Controller;