import { Router } from 'express';

import { authenticate } from '../../middleware';
import upload from '../../libs/multer';
import { Attendance as AttendaceRepository } from '../../models';
import Controller from './controller';

const basePath = `/api/v1`;
const setPath = path => `${basePath}/${path}`;
const routes = Router();
const controller = Controller({ AttendaceRepository });

routes
    .route(setPath('checkin'))
    .post(authenticate.jwt, upload.single('selfie'), controller.checkin);

routes
    .route(setPath('checkout'))
    .post(authenticate.jwt, upload.single('selfie'), controller.checkout);

export default routes;
