import { Router } from 'express';

import { ClientCompany as ClientCompanyRepository } from '../../models';
import Controller from './controller';

const basePath = `/api/v1/client-company`;
const setPath = path => `${basePath}/${path}`;
const routes = Router();
const controller = Controller({ ClientCompanyRepository });

routes
    .route(basePath)
    .get(controller.index)
    .post(controller.store);

routes
    .route(setPath(':id'))
    .get(controller.show)
    .put(controller.update)
    .delete(controller.destroy);

export default routes;
