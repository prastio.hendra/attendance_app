const Controller = repositories => {
    const { ClientCompanyRepository } = repositories;

    const index = async (req, res, next) => {
        try {
            const clientCompanies = await ClientCompanyRepository.findAll({})
            res.status(200).json({
                data: clientCompanies,
                code: 200,
                message: 'Success'
            });
        } catch (err) {
            next(err)
        }
    }

    const store = async (req, res, next) => {
        const { body } = req;
        try {
            const [clientCompany, created] = await ClientCompanyRepository.findOrCreate({ where: body });
            if(created) {
                res.status(201).json({
                    data: clientCompany,
                    code: 201,
                    message: 'Client Company created successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'The Client Company already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }

    const show = async (req, res, next) => {
        const { id } = req.params;
        try {
            const clientCompany = await ClientCompanyRepository.findByPk(id);
            if(!clientCompany) {
                res.status(404).json({
                    code: 404,
                    message: "Client Company doesn't exists"
                });
            } else {
                res.status(200).json({
                    data: clientCompany,
                    code: 200,
                    message: 'Success'
                });
            }
        } catch (err) {
            next(err);
        }
    }

    const update = async (req, res, next) => {
        const { id } = req.params;
        const { body } = req;

        try {
            const clientCompanyExists = await ClientCompanyRepository.findByPk(id)
            if (clientCompanyExists) {
                const updatedExpertise = await clientCompanyExists.update({ ...body, id }, { returning: true, plain: true });
                res.status(200).json({
                    data: { ...updatedExpertise.dataValues },
                    code: 200,
                    message: 'Success'
                });
            } else {
                res.status(404).json({ code: 404, message: "Client Company doesn't exists!" })
            }
        } catch (err) {
            next(err);
        }
    }

    const destroy = async (req, res, next) => {
        const { id } = req.params;
        try {
            const clientCompanyExists = await ClientCompanyRepository.findByPk(id)

            if (clientCompanyExists) {
                await clientCompanyExists.destroy()
                res.status(200).json({ code: 200, message: 'Client Company deleted successfully.' });
            } else {
                res.status(404).json({ message: `There is no Client Company with id ${id}` });
            }
        } catch (err) {
            next(err)
        }
    }

    return {
        index,
        store,
        show,
        update,
        destroy
    }
}

export default Controller;