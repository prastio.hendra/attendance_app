import httpStatus from 'http-status';

const AuthController = services => {
  const { authService } = services;

  const login = async (req, res, next) => {
    const { username, password } = req.body;

    try {
      const token = await authService.login(username, password);
      res.send({ data: token, code: 200, message: 'Success' });
    } catch (err) {
      next(err);
    }
  };

  const logout = async (req, res, next) => {
    const { user } = req;

    try {
      authService.logout(user);
      res.status(httpStatus.NO_CONTENT).send();
    } catch (err) {
      next(err);
    }
  };

  return {
    login,
    logout,
  };
};

export default AuthController;
