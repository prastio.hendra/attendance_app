import { Joi } from 'express-validation';

export default {
  login: {
    body: Joi.object().keys({
      username: Joi.string().required(),
      password: Joi.string().required(),
    }),
  },
  logout: {
    body: Joi.object().keys({
      imei: Joi.string().optional(),
    }),
  },
};
