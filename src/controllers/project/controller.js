const Controller = repositories => {
    const { ProjectRepository } = repositories;

    const index = async (req, res, next) => {
        try {
            const projects = await ProjectRepository.findAll({})
            res.status(200).json({
                data: projects,
                code: 200,
                message: 'Success'
            });
        } catch (err) {
            next(err)
        }
    }

    const store = async (req, res, next) => {
        const { body } = req;
        try {
            const [project, created] = await ProjectRepository.findOrCreate({ where: body });
            if(created) {
                res.status(201).json({
                    data: project,
                    code: 201,
                    message: 'Project created successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'The Project already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }

    const show = async (req, res, next) => {
        const { id } = req.params;
        try {
            const project = await ProjectRepository.findByPk(id);
            if(!project) {
                res.status(404).json({
                    code: 404,
                    message: "Project doesn't exists"
                });
            } else {
                res.status(200).json({
                    data: project,
                    code: 200,
                    message: 'Success'
                });
            }
        } catch (err) {
            next(err);
        }
    }

    const update = async (req, res, next) => {
        const { id } = req.params;
        const { body } = req;

        try {
            const projectExists = await ProjectRepository.findByPk(id)
            if (projectExists) {
                const updatedProject = await projectExists.update({ ...body, id }, { returning: true, plain: true });
                res.status(200).json({
                    data: { ...updatedProject.dataValues },
                    code: 200,
                    message: 'Success'
                });
            } else {
                res.status(404).json({ code: 404, message: "Project doesn't exists!" })
            }
        } catch (err) {
            next(err);
        }
    }

    const destroy = async (req, res, next) => {
        const { id } = req.params;
        try {
            const projectExists = await ProjectRepository.findByPk(id)

            if (projectExists) {
                await projectExists.destroy()
                res.status(200).json({ code: 200, message: 'Project deleted successfully.' });
            } else {
                res.status(404).json({ message: `There is no Project with id ${id}` });
            }
        } catch (err) {
            next(err)
        }
    }

    return {
        index,
        store,
        show,
        update,
        destroy
    }
}

export default Controller;