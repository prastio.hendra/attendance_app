const Controller = repositories => {
    const { ProjectTeamRepository } = repositories;

    const index = async (req, res, next) => {
        try {
            const projectTeams = await ProjectTeamRepository.findAll({})
            res.status(200).json({
                data: projectTeams,
                code: 200,
                message: 'Success'
            });
        } catch (err) {
            next(err)
        }
    }

    const store = async (req, res, next) => {
        const { body } = req;
        try {
            const [projectTeam, created] = await ProjectTeamRepository.findOrCreate({ where: body });
            if(created) {
                res.status(201).json({
                    data: projectTeam,
                    code: 201,
                    message: 'Project Teams created successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'The Project Teams already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }

    const show = async (req, res, next) => {
        const { id } = req.params;
        try {
            const projectTeam = await ProjectTeamRepository.findByPk(id);
            if(!projectTeam) {
                res.status(404).json({
                    code: 404,
                    message: "Project Teams doesn't exists"
                });
            } else {
                res.status(200).json({
                    data: projectTeam,
                    code: 200,
                    message: 'Success'
                });
            }
        } catch (err) {
            next(err);
        }
    }

    const update = async (req, res, next) => {
        const { id } = req.params;
        const { body } = req;

        try {
            const projectTeamExists = await ProjectTeamRepository.findByPk(id)
            if (projectTeamExists) {
                const updatedProjectTeam = await projectTeamExists.update({ ...body, id }, { returning: true, plain: true });
                res.status(200).json({
                    data: { ...updatedProjectTeam.dataValues },
                    code: 200,
                    message: 'Success'
                });
            } else {
                res.status(404).json({ code: 404, message: "Project Teams doesn't exists!" })
            }
        } catch (err) {
            next(err);
        }
    }

    const destroy = async (req, res, next) => {
        const { id } = req.params;
        try {
            const projectTeamExists = await ProjectTeamRepository.findByPk(id)

            if (projectTeamExists) {
                await projectTeamExists.destroy()
                res.status(200).json({ code: 200, message: 'Project Teams deleted successfully.' });
            } else {
                res.status(404).json({ message: `There is no Project Teams with id ${id}` });
            }
        } catch (err) {
            next(err)
        }
    }

    return {
        index,
        store,
        show,
        update,
        destroy
    }
}

export default Controller;