const Controller = repositories => {
    const { ExpertiseRepository } = repositories;

    const index = async (req, res, next) => {
        try {
            const expertises = await ExpertiseRepository.findAll({})
            res.status(200).json({
                data: expertises,
                code: 200,
                message: 'Success'
            });
        } catch (err) {
            next(err)
        }
    }

    const store = async (req, res, next) => {
        const { body } = req;
        try {
            const [expertise, created] = await ExpertiseRepository.findOrCreate({ where: body });
            if(created) {
                res.status(201).json({
                    data: expertise,
                    code: 201,
                    message: 'Expertise created successfully'
                });
            } else {
                res.status(500).json({
                    code: 500,
                    message: 'The expertise already exists.'
                });
            }
        } catch (err) {
            next(err)
        }
    }

    const show = async (req, res, next) => {
        const { id } = req.params;
        try {
            const expertise = await ExpertiseRepository.findByPk(id);
            if(!expertise) {
                res.status(404).json({
                    code: 404,
                    message: "Expertise doesn't exists"
                });
            } else {
                res.status(200).json({
                    data: expertise,
                    code: 200,
                    message: 'Success'
                });
            }
        } catch (err) {
            next(err);
        }
    }

    const update = async (req, res, next) => {
        const { id } = req.params;
        const { body } = req;

        try {
            const expertiseExists = await ExpertiseRepository.findByPk(id)
            if (expertiseExists) {
                const updatedExpertise = await expertiseExists.update({ ...body, id }, { returning: true, plain: true });
                res.status(200).json({
                    data: { ...updatedExpertise.dataValues },
                    code: 200,
                    message: 'Success'
                });
            } else {
                res.status(404).json({ code: 404, message: "Expertise doesn't exists!" })
            }
        } catch (err) {
            next(err);
        }
    }

    const destroy = async (req, res, next) => {
        const { id } = req.params;
        try {
            const ExpertiseExists = await ExpertiseRepository.findByPk(id)

            if (ExpertiseExists) {
                await ExpertiseExists.destroy()
                res.status(200).json({ code: 200, message: 'Expertise deleted successfully.' });
            } else {
                res.status(404).json({ message: `There is no expertise with id ${id}` });
            }
        } catch (err) {
            next(err)
        }
    }

    return {
        index,
        store,
        show,
        update,
        destroy
    }
}

export default Controller;