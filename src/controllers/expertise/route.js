import { Router } from 'express';

import { Expertise as ExpertiseRepository } from '../../models';
import Controller from './controller';

const basePath = `/api/v1/expertise`;
const setPath = path => `${basePath}/${path}`;
const routes = Router();
const controller = Controller({ ExpertiseRepository });

routes
    .route(basePath)
    .get(controller.index)
    .post(controller.store);

routes
    .route(setPath(':id'))
    .get(controller.show)
    .put(controller.update)
    .delete(controller.destroy);

export default routes;
