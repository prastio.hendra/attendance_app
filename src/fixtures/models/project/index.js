// ========================================
// PROJECT
// ========================================

export const PROJECT_TABLE_NAME = 'Projects';
export const PROJECT_MODEL_NAME = 'Project';

export * from './relations';