// ========================================
// PROJECT CLIENT COMPANY
// ========================================

export const PROJECT_BELONGS_TO_CLIENT_COMPANY = { as: 'company' };

// ========================================
// PROJECT EMPLOYEE
// ========================================

export const PROJECT_BELONGS_TO_MANY_EMPLOYEE = { 
    foreignKey: 'projectId',
    as: 'employees',
    through: 'ProjectTeams',
};