// ========================================
// USER
// ========================================

export const USER_TABLE_NAME = 'Users';
export const USER_MODEL_NAME = 'User';
export const MODEL_BELONGS_TO_USER = { foreignKey: 'userId', as: 'user' };

export * from './relations';