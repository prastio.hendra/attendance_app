// ========================================
// EXPERTISE
// ========================================

export const EXPERTISE_TABLE_NAME = 'Expertises';
export const EXPERTISE_MODEL_NAME = 'Expertise';

export * from './relations';