// ========================================
// EXPERTISE EMPLOYEE
// ========================================

export const EXPERTISE_BELONGS_TO_MANY_EMPLOYEE = {
    foreignKey: 'expertiseId',
    as: 'employees',
    through: 'ExpertisesEmployees',
};