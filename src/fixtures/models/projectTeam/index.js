// ========================================
// PROJECT TEAM
// ========================================

export const PROJECT_TEAM_TABLE_NAME = 'ProjectTeams';
export const PROJECT_TEAM_MODEL_NAME = 'ProjectTeam';