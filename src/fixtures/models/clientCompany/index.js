// ========================================
// CLIENT COMPANY
// ========================================

export const CLIENT_COMPANY_TABLE_NAME = 'ClientCompanies';
export const CLIENT_COMPANY_MODEL_NAME = 'ClientCompany';

export * from './relations';