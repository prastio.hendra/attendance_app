// ========================================
// EMPLOYEE USER
// ========================================

export const EMPLOYEE_HAS_ONE_USER = {
    foreignKey: 'employeeId',
    as: 'user',
};

// ========================================
// EMPLOYEE ATTENDANCE
// ========================================

export const EMPLOYEE_HAS_MANY_ATTENDANCE = { 
    foreignKey: 'employeeId',
    as: 'attendances',
};

// ========================================
// EMPLOYEE EXPERTISE
// ========================================

export const EMPLOYEE_BELONGS_TO_MANY_EXPERTISE = { 
    foreignKey: 'employeeId',
    as: 'expertises',
    through: 'ExpertisesEmployees',
};

// ========================================
// EMPLOYEE PROJECT
// ========================================

export const EMPLOYEE_BELONGS_TO_MANY_PROJECT = { 
    foreignKey: 'employeeId',
    as: 'projects',
    through: 'ProjectTeams',
};