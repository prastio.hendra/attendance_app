// ========================================
// EMPLOYEE
// ========================================

export const EMPLOYEE_TABLE_NAME = 'Employees';
export const EMPLOYEE_MODEL_NAME = 'Employee';
export const MODEL_BELONGS_TO_EMPLOYEE = { foreignKey: 'employeeId', as: 'employee' };

export * from './relations';