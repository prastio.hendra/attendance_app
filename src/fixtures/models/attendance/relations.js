// ========================================
// ATTENDANCE EMPLOYEE
// ========================================

export const ATTENDANCE_BELONGS_TO_EMPLOYEE = {
    foreignKey: 'employeeId',
    as: 'user',
};