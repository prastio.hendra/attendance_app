export const FOREIGN_KEY_CONSTRAINT = additionalConstaints => ({
  onUpdate: 'CASCADE',
  onDelete: 'CASCADE',
  constraints: true,
  ...additionalConstaints,
});

export * from './user';
export * from './employee';
export * from './attendance';
export * from './expertise';
export * from './expertisesEmployee';
export * from './clientCompany';
export * from './project';
export * from './projectTeam';