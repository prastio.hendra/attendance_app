import Sequelize from 'sequelize';
import { v4 as uuidv4 } from 'uuid';

export const schemaAttributes = {
  id: {
    type: Sequelize.UUID,
    defaultValue: uuidv4(),
    primaryKey: true,
  },
  employeeCode: {
    type: Sequelize.STRING
  },
  fullName: {
    type: Sequelize.STRING
  },
  idCardNo: {
    type: Sequelize.STRING
  },
  taxCardNo: {
    type: Sequelize.STRING
  },
  joinAt: {
    type: Sequelize.DATE
  },
  email: {
    type: Sequelize.STRING
  },
  phone: {
    type: Sequelize.STRING
  },
  bod: {
    type: Sequelize.DATE
  },
  pob: {
    type: Sequelize.STRING
  },
  idCardAddress: {
    type: Sequelize.STRING
  },
  currentAddress: {
    type: Sequelize.STRING
  },
  emergencyContactName: {
    type: Sequelize.STRING
  },
  emergencyContact: {
    type: Sequelize.STRING
  },
  createdAt: {
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE
  }
};

const tableName = 'Employees';

export default {
  up: queryInterface => queryInterface.createTable(tableName, schemaAttributes, { schema: 'public' }),
  down: queryInterface => queryInterface.dropTable({ tableName, schema: 'public' }),
};