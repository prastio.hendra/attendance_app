import Sequelize from 'sequelize';
import { v4 as uuidv4 } from 'uuid';

export const schemaAttributes = {
  id: {
    type: Sequelize.UUID,
    defaultValue: uuidv4(),
    allowNull: false,
    primaryKey: true,
  },
  username: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
  email: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true
  },
  employeeId: {
    allowNull: true,
    type: Sequelize.UUID
  },
  role: {
    type: Sequelize.INTEGER
  },
  createdAt: {
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE
  }
};

const tableName = 'Users';

export default {
  up: queryInterface => queryInterface.createTable(tableName, schemaAttributes, { schema: 'public' }),
  down: queryInterface => queryInterface.dropTable({ tableName, schema: 'public' }),
};