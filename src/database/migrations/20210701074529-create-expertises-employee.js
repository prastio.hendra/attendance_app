import Sequelize from 'sequelize';
import { v4 as uuidv4 } from 'uuid';

export const schemaAttributes = {
  id: {
    type: Sequelize.UUID,
    defaultValue: uuidv4(),
    primaryKey: true,
  },
  employeeId: {
    type: Sequelize.UUID
  },
  expertiseId: {
    type: Sequelize.UUID
  },
  createdAt: {
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE
  }
};

const tableName = 'ExpertisesEmployees';

export default {
  up: queryInterface => queryInterface.createTable(tableName, schemaAttributes, { schema: 'public' }),
  down: queryInterface => queryInterface.dropTable({ tableName, schema: 'public' }),
};