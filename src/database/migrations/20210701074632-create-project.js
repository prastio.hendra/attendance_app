import Sequelize from 'sequelize';
import { v4 as uuidv4 } from 'uuid';

export const schemaAttributes = {
  id: {
    type: Sequelize.UUID,
    defaultValue: uuidv4(),
    primaryKey: true,
  },
  clientCompanyId: {
    type: Sequelize.UUID
  },
  name: {
    type: Sequelize.STRING
  },
  startDate: {
    type: Sequelize.DATE
  },
  endDate: {
    type: Sequelize.DATE
  },
  actualStartDate: {
    type: Sequelize.DATE
  },
  actualEndDate: {
    type: Sequelize.DATE
  },
  createdAt: {
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE
  }
};

const tableName = 'Projects';

export default {
  up: queryInterface => queryInterface.createTable(tableName, schemaAttributes, { schema: 'public' }),
  down: queryInterface => queryInterface.dropTable({ tableName, schema: 'public' }),
};