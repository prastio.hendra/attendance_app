import bcrypt from 'bcrypt';

'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', [{
      username: 'john_doe',
      password: bcrypt.hashSync('test123', bcrypt.genSaltSync(8)),
      email: 'john_doe@bts.dev',
      role: 2
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
     await queryInterface.bulkDelete('Users', null, {});
  }
};
