'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Employees', [{
      employeeCode: '0817218',
      fullName: 'John Doe',
      idCardNo: '1000',
      taxCardNo: '1000',
      joinAt: new Date('December 17, 2020 10:24:00'),
      email: 'john_doe@bts.dev',
      phone: '089512345xxx',
      bod: new Date('May 05, 1956 22:06:00'),
      pob: 'Bandung',
      idCardAddress: '1000',
      currentAddress: 'Jl. Rames No.20',
      emergencyContactName: 'Rony',
      emergencyContact: '089545678xxx',
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Employees', null, {});
  }
};
