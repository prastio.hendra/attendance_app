/* eslint-disable global-require */
/* eslint-disable no-param-reassign */

import fs from 'fs';
import path from 'path';

import Sequelize from 'sequelize';

import config from '../config';

const { dbName, dbUsername, dbPassword, dbHost, dbDialect, dbEnableLogging, dbDebug, dbSchema } = config;

const basename = path.basename(__filename);
// eslint-disable-next-line no-console
const dbLogging = JSON.parse(dbEnableLogging) ? console.log : false;

const sequelize = new Sequelize(dbName, dbUsername, dbPassword, {
  host: dbHost,
  dialect: dbDialect,
  timezone: '+07:00',
  logging: dbLogging,
  DEBUG: dbDebug,
  define: {
    schema: dbSchema,
    searchPath: dbSchema,
    timestamps: true,
  },
});

const db = {};

fs.readdirSync(__dirname)
  .filter(file => file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js')
  .forEach(file => {
    // eslint-disable-next-line import/no-dynamic-require
    const model = require(path.join(__dirname, file)).default(sequelize, Sequelize);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName]?.associate) {
    db[modelName].associate(db);
  }
});


Sequelize.postgres.DATE.parse = value => value.toLocaleString();

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
