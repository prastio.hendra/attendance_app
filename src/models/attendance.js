import { v4 as uuidv4 } from 'uuid';

import {
  EMPLOYEE_MODEL_NAME,
  ATTENDACE_MODEL_NAME as modelName,
  ATTENDACE_TABLE_NAME as tableName,
  ATTENDANCE_BELONGS_TO_EMPLOYEE
} from '../fixtures/models';

export default (sequelize, DataTypes) => {
  const schemaAttributes = {
    id: { type: DataTypes.UUID, defaultValue: uuidv4(), primaryKey: true },
    employeeId: { type: DataTypes.UUID },
    checkin: { type: DataTypes.DATE },
    checkout: { type: DataTypes.DATE },
    selfie: { type: DataTypes.STRING },
    checkinLat: { type: DataTypes.DOUBLE },
    checkinLng: { type: DataTypes.DOUBLE },
    checkoutLat: { type: DataTypes.DOUBLE },
    checkoutLng: { type: DataTypes.DOUBLE }
  };

  const Attendance = sequelize.define(modelName, schemaAttributes, { tableName });

  Attendance.associate = models => {
    Attendance.belongsTo(models[EMPLOYEE_MODEL_NAME], ATTENDANCE_BELONGS_TO_EMPLOYEE);
  };

  return Attendance;
};