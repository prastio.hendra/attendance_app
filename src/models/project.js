import { v4 as uuidv4 } from 'uuid';

import {
  CLIENT_COMPANY_MODEL_NAME,
  EMPLOYEE_MODEL_NAME,
  PROJECT_BELONGS_TO_CLIENT_COMPANY,
  PROJECT_BELONGS_TO_MANY_EMPLOYEE,
  PROJECT_MODEL_NAME as modelName,
  PROJECT_TABLE_NAME as tableName
} from '../fixtures/models';


export default (sequelize, DataTypes) => {
  const schemaAttributes = {
    id: { type: DataTypes.UUID, defaultValue: uuidv4(), primaryKey: true },
    clientCompanyId: { type: DataTypes.UUID },
    name: { type: DataTypes.STRING },
    startDate: { type: DataTypes.DATE },
    endDate: { type: DataTypes.DATE },
    actualStartDate: { type: DataTypes.DATE },
    actualEndDate: { type: DataTypes.DATE }
  };

  const Project = sequelize.define(modelName, schemaAttributes, { tableName });

  Project.associate = models => {
    Project.belongsTo(models[CLIENT_COMPANY_MODEL_NAME], PROJECT_BELONGS_TO_CLIENT_COMPANY);
    Project.belongsToMany(models[EMPLOYEE_MODEL_NAME], PROJECT_BELONGS_TO_MANY_EMPLOYEE);
  }

  return Project;
};