import { v4 as uuidv4 } from 'uuid';

import {
  EMPLOYEE_MODEL_NAME,
  EXPERTISE_BELONGS_TO_MANY_EMPLOYEE,
  EXPERTISE_MODEL_NAME as modelName,
  EXPERTISE_TABLE_NAME as tableName,
} from '../fixtures/models';

export default (sequelize, DataTypes) => {
  const schemaAttributes = {
    id: { type: DataTypes.UUID, defaultValue: uuidv4(), primaryKey: true },
    name: { type: DataTypes.STRING }
  };

  const Expertise = sequelize.define(modelName, schemaAttributes, { tableName });

  Expertise.associate = models => {
    Expertise.belongsToMany(models[EMPLOYEE_MODEL_NAME], EXPERTISE_BELONGS_TO_MANY_EMPLOYEE);
  };

  return Expertise;
};