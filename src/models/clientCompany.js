import { v4 as uuidv4 } from 'uuid';

import {
  PROJECT_MODEL_NAME,
  CLIENT_COMPANY_HAS_MANY_PROJECT,
  CLIENT_COMPANY_MODEL_NAME as modelName,
  CLIENT_COMPANY_TABLE_NAME as tableName
} from '../fixtures/models';

export default (sequelize, DataTypes) => {
  const schemaAttributes = {
    id: { type: DataTypes.UUID, defaultValue: uuidv4(), primaryKey: true },
    companyName: { type: DataTypes.STRING },
  };

  const ClientCompany = sequelize.define(modelName, schemaAttributes, { tableName });

  ClientCompany.associate = models => {
    ClientCompany.hasMany(models[PROJECT_MODEL_NAME], CLIENT_COMPANY_HAS_MANY_PROJECT);
  }

  return ClientCompany;
};