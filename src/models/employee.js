import { v4 as uuidv4 } from 'uuid';

import {
  USER_MODEL_NAME,
  EXPERTISE_MODEL_NAME,
  EMPLOYEE_HAS_ONE_USER,
  ATTENDACE_MODEL_NAME,
  EMPLOYEE_HAS_MANY_ATTENDANCE,
  EMPLOYEE_BELONGS_TO_MANY_EXPERTISE,
  EMPLOYEE_MODEL_NAME as modelName,
  EMPLOYEE_TABLE_NAME as tableName,
  PROJECT_TEAM_MODEL_NAME,
  EMPLOYEE_BELONGS_TO_MANY_PROJECT,
} from '../fixtures/models';

export default (sequelize, DataTypes) => {
  const schemaAttributes = {
    id: { type: DataTypes.UUID, defaultValue: uuidv4(), allowNull: false, primaryKey: true },
    employeeCode: { type: DataTypes.STRING },
    fullName: { type: DataTypes.STRING },
    idCardNo: { type: DataTypes.STRING },
    taxCardNo: { type: DataTypes.STRING },
    joinAt: { type: DataTypes.DATE },
    email: { type: DataTypes.STRING },
    phone: { type: DataTypes.STRING },
    bod: { type: DataTypes.DATE },
    pob: { type: DataTypes.STRING },
    idCardAddress: { type: DataTypes.STRING },
    currentAddress: { type: DataTypes.STRING },
    emergencyContactName: { type: DataTypes.STRING },
    emergencyContact: { type: DataTypes.STRING },
  };

  const Employee = sequelize.define(modelName, schemaAttributes, { tableName });

  Employee.associate = models => {
    Employee.hasOne(models[USER_MODEL_NAME], EMPLOYEE_HAS_ONE_USER);
    Employee.hasMany(models[ATTENDACE_MODEL_NAME], EMPLOYEE_HAS_MANY_ATTENDANCE);
    Employee.belongsToMany(models[EXPERTISE_MODEL_NAME], EMPLOYEE_BELONGS_TO_MANY_EXPERTISE);
    Employee.belongsToMany(models[PROJECT_TEAM_MODEL_NAME], EMPLOYEE_BELONGS_TO_MANY_PROJECT);
  };

  return Employee;
};