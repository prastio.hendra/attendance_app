import { v4 as uuidv4 } from 'uuid';

import {
  PROJECT_TEAM_MODEL_NAME as modelName,
  PROJECT_TEAM_TABLE_NAME as tableName
} from '../fixtures/models';

export default (sequelize, DataTypes) => {
  const schemaAttributes = {
    id: { type: DataTypes.UUID, defaultValue: uuidv4(), primaryKey: true },
    employeeId: { allowNull: true, type: DataTypes.UUID },
    projectId: { type: DataTypes.UUID },
    position: { type: DataTypes.STRING },
    startDate: { type: DataTypes.DATE },
    endDate: { type: DataTypes.DATE },
  };

  const ProjectTeam = sequelize.define(modelName, schemaAttributes, { tableName });

  return ProjectTeam;
};
