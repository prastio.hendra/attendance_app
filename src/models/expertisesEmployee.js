import { v4 as uuidv4 } from 'uuid';

import {
  EXPERTISES_EMPLOYEE_MODEL_NAME as modelName,
  EXPERTISE_TABLE_NAME as tableName
} from '../fixtures/models';

export default (sequelize, DataTypes) => {
  const schemaAttributes = {
    id: { type: DataTypes.UUID, defaultValue: uuidv4(), primaryKey: true },
    employeeId: { type: DataTypes.UUID },
    expertiseId: { type: DataTypes.UUID }
  };

  const ExpertisesEmployee = sequelize.define(modelName, schemaAttributes, { tableName });

  return ExpertisesEmployee;
};