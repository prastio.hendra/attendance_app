import bcrypt from 'bcrypt';
import { v4 as uuidv4 } from 'uuid'

import {
  EMPLOYEE_MODEL_NAME,
  USER_BELONGS_TO_EMPLOYEE,
  USER_MODEL_NAME as modelName,
  USER_TABLE_NAME as tableName
} from '../fixtures/models'

export default (sequelize, DataTypes) => {
  const schemaAttributes = {
    id: { type: DataTypes.UUID, defaultValue: uuidv4(), allowNull: false, primaryKey: true },
    username: { allowNull: false, unique: true, type: DataTypes.STRING },
    password: {
      type: DataTypes.STRING,
      set(password) {
        this.setDataValue('password', bcrypt.hashSync(password, bcrypt.genSaltSync(8)));
      },
    },
    email: { unique: true, type: DataTypes.STRING },
    employeeId: { type: DataTypes.UUID },
    role: { type: DataTypes.INTEGER },
  };

  const defaultScope = { attributes: { exclude: ['password'] } };

  const User = sequelize.define(modelName, schemaAttributes, { tableName, defaultScope });

  User.associate = models => {
    User.belongsTo(models[EMPLOYEE_MODEL_NAME], USER_BELONGS_TO_EMPLOYEE);
  };

  return User;
};
